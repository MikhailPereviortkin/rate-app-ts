import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { getCalculatedSumm } from './actions/getActiveRate';
import './App.scss';
export interface IState {
  buyDate: string,
  summ: number,
}

export interface IProps {
  onGetCalculatedSumm?: (buyDate: any, summ: any) => Promise<any>;
  exchangeRate?: any;
}

class App extends React.Component<IProps, IState>  {

  public constructor(props: Readonly<IProps>) {
    super(props);

    this.state = {
      buyDate: new Date().toDateString(),
      summ: 0,
    };

    this.handleCalculateRate = this.handleCalculateRate.bind(this);

  }

  public render() {
    return (
      <div className="form-wrapper">
        <div className="field">
          <label className="form-label" >Date:</label>
          <input className="input-field" id="buyDate" type="date" value={this.state.buyDate} onChange={this.handleDateChange} />
        </div>
        <div className="field">
          <label className="form-label" htmlFor={'summ'}>Amount USD:</label>
          <input className="input-field" id="summ" onChange={this.handleSummChange} value={this.state.summ} />
        </div>
        <div className="recalculate-row">
          <input className="recalculate-button" type="button" value="Recalculate" onClick={this.handleCalculateRate} />
        </div>
        <div className="result-field">
          <input className={'result-' + (this.props.exchangeRate > 0 ? 'plus' : this.props.exchangeRate < 0 ? 'minus' : 'zero')} disabled={true} value={Number(this.props.exchangeRate).toFixed(2)} />
          <div>&#10240; рублей</div>
        </div>
      </div>
    );
  }

  private handleCalculateRate() {
    const { onGetCalculatedSumm } = this.props;
    if (onGetCalculatedSumm != undefined) {
      onGetCalculatedSumm(this.state.buyDate, this.state.summ);
    }
  }

  private handleSummChange = (event: any) => {
    const { value } = event.target;
    this.setState({ summ: value });
  }

  private handleDateChange = (event: any) => {
    const { value } = event.target;
    this.setState({ buyDate: value });
  }

}

const mapStateToProps = (state: any) => {
  return {
    exchangeRate: state.default.exchangeRate.data,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<any>): any => {
  return {
    onGetCalculatedSumm: bindActionCreators(getCalculatedSumm, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);