import axios from 'axios';

// tslint:disable-next-line:no-namespace
export namespace RequestType {
  export const GET = 'get';
}

export interface IRequestParams {
  method: string;
  url: string;
}

export const sendRequest = (params: IRequestParams): Promise<any> => {
  const { method, url } = params;

  if (!method) {
    throw new Error('Method could not be empty, please check arguments of sendRequest function');
  }

  if (!url) {
    throw new Error('Url could not be empty, please check arguments of sendRequest function');
  }

  const requestArguments = {
    method,
    url : '',
  };

  requestArguments.url = decodeURI(url);

  return axios(requestArguments);
};
