import * as moment from 'moment';
import { Dispatch } from 'redux';
import { RequestType, sendRequest } from '../ajax';
import {
  RATE_ENDPOINT
} from '../constants/endpoints';
import settings from '../constants/settings'
import { createActionObject } from './';

export const EXCHANGE_RATE_REQUEST = '@/EXCHANGE_RATE_REQUEST';
export const EXCHANGE_RATE_RESPONSE = '@/EXCHANGE_RATE_RESPONSE';
export const EXCHANGE_RATE_RESPONSE_FAIL = '@/EXCHANGE_RATE_RESPONSE_FAIL';

const exchangeRateRequest = () => {
  return createActionObject(EXCHANGE_RATE_REQUEST);
};

const exchangeRateResponse = (data: any) => {
  return createActionObject(EXCHANGE_RATE_RESPONSE, { ...data });
};

const exchangeRateResponseFail = (data: any) => {
  return createActionObject(EXCHANGE_RATE_RESPONSE_FAIL, { ...data });
};

export function getCalculatedSumm(date: any, summ: any): (dispatch: Dispatch<any>) => Promise<any> {
  return async (dispatch: Dispatch<any>) => {
    dispatch(exchangeRateRequest());
    const extUrl = '?base=' + settings.maps.base + '&symbols=' + settings.maps.symbols;
    let profitSpread = 0;
    try {
      const formatDate = (moment(date).format(settings.maps.dateFormat));

      const responseLatest = await sendRequest({
        method: RequestType.GET,
        url: RATE_ENDPOINT + 'latest' + extUrl,
      });
      const responseSelected = await sendRequest({
        method: RequestType.GET,
        url: RATE_ENDPOINT + formatDate + extUrl,
      });

      if (responseLatest.status === 200 && responseSelected.status === 200) {
        const latestRate = responseLatest.data.rates.RUB;
        const selectedRate = responseSelected.data.rates.RUB;
        const profit = latestRate * summ - selectedRate * summ;

        profitSpread = profit - profit * settings.maps.spread / 100;
      }

      return dispatch(exchangeRateResponse({
        result: profitSpread
      }));
    } catch (error) {
      return dispatch(exchangeRateResponseFail({ error }));
    }
  }
}  