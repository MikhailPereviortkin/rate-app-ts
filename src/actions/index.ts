import { Action } from 'redux';

export const createActionObject = (type: string, props?: any): Action => {
  return {
    type,
    ...props,
  };
};
