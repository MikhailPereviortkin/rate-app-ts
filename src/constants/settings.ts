export default {
    maps: {
      base: 'USD',
      dateFormat: 'YYYY-MM-DD',
      spread: 0.5,
      symbols: 'RUB',
    },
  };
  