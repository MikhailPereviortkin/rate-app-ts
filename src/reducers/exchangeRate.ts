import {
  EXCHANGE_RATE_REQUEST,
  EXCHANGE_RATE_RESPONSE,
  EXCHANGE_RATE_RESPONSE_FAIL,
} from '../actions/getActiveRate';


const initialState = {
  data: 0,
  isLoaded: false,
  isLoading: false,
  isRequested: false,
};

export default function reducer(state = initialState, action: any) {
  switch (action.type) {
    case EXCHANGE_RATE_REQUEST:
      return {
        ...state,
        isLoaded: false,
        isLoading: true,
        isRequested: true,
      };

    case EXCHANGE_RATE_RESPONSE:
      return {
        ...state,
        data: action.result,
        isLoaded: true,
        isLoading: false,
        isRequested: true,
      };
    case EXCHANGE_RATE_RESPONSE_FAIL: return {
      eror: action.error,
      ...state,
    };
    default: return state;
  }
}
